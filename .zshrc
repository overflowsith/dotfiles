# Load version control information
autoload -Uz compinit && compinit
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats 'on branch %b'

setopt prompt_subst histignorealldups
PROMPT="
%F{008}%n%f %F{012}%~%f %F{011}\$vcs_info_msg_0_%f
%(!.#.❯) "
zstyle ':vcs_info:git:*' formats '%b'

if [[ -d ~/.zshsrc ]]; then
  for rc in ~/.zshsrc/*.zsh; do
    source "$rc"
  done
  unset rc;
fi

