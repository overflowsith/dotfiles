alias ll="ls -lhG"
alias la="ls -laG"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias mkdir="mkdir -pv"

alias gophp72="brew unlink php && brew link php@7.2"
alias gophp74="brew unlink php && brew link php@7.4"
alias gophp80="brew unlink php && brew link php@8.0"
alias gophp81="brew unlink php && brew link php@8.1"

alias wget="wget -c"

alias artisan="php artisan"
alias cons="bin/console"
alias pi="php -S localhost:8765"

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias dsrem='find . -name ".DS_Store" -type f -print -delete'
