export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

if [ -d ~/.local/bin ] ; then
    PATH=~/.local/bin:$PATH
fi

if [ -d ~/.composer/vendor/bin ] ; then
    PATH=~/.composer/vendor/bin:$PATH
fi

if [ -d ~/dev/tools/flutter/bin ] ; then
    PATH=~/dev/tools/flutter/bin:$PATH
fi

if [ -d "/Applications/Visual Studio Code.app/Contents/Resources/app/bin" ] ; then
    PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin":$PATH
fi

if [ -d "~/.nvm/versions/node/v10.24.1/bin" ] ; then
    PATH="~/.nvm/versions/node/v10.24.1/bin":$PATH
fi

